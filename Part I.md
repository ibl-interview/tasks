## Part 1:
1. It is hard to pick one particular achievement. However, If i would have to pick one, I would say it would be a Lua based assembly test generator for the Tachyum Prodigy HPC CPU.
2. There definitely are choices I made, that after looking back were not optimal, they made me the person I am today. IMO it is pointless agonizing over past choices, because we cannot change them. Best we can do is look back and learn from them.
3. I've always enjoyed developing meta tools and CLI applications, and up until recently I haven't fully realized how much I miss it. The Support Engineer position @ IBL would allow me to do just that.
## Part 2
1. By running the `ip address` command in a terminal.
2. a
3. b
4. c
5. d
6. Well, 1st of all I would need to know the version of the app they were running and the environment the app was running on. Then I would check if it is a known and documented bug within the application. If not I would ask about details how the bug occurred to, possibly, be able to reproduce it, analyze it and offer support on how to circumvent or better yet fix it.
7. 